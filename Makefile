
all :

clean :

install-nodejs :
	sudo yum install nodejs

install-npm :
	sudo yum install npm

install-karma :
	sudo npm install -g karma

install-git :
	sudo yum install git

angular-phonecat.git :
	git clone git://github.com/angular/angular-phonecat.git